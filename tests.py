from unittest.mock import patch

import pytest

from game import Game


@pytest.fixture
def game():
    return Game()


def setup_board(game, positions, color):
    for row, col in positions:
        game.board[row][col] = color


def test_insert_ball_success(game):
    success = game.insert_ball(column=1, color="R")
    assert success
    assert game.board[5][0]


def test_insert_ball_full_column(game):
    for _ in range(len(game.ROWS)):
        game.insert_ball(column=1, color="Y")
    assert not game.insert_ball(column=1, color="R")


def test_insert_ball_boundary_conditions(game):
    success_leftmost = game.insert_ball(column=1, color="R")
    success_rightmost = game.insert_ball(column=len(game.COLUMNS), color="Y")
    assert success_leftmost
    assert success_rightmost
    assert game.board[5][0] == "R"
    assert game.board[5][len(game.COLUMNS) - 1] == "Y"


def test_horizontal_victory(game):
    setup_board(game, [(0, 0), (0, 1), (0, 2), (0, 3)], "R")
    assert game.check_horizontal_victory() == "R"


def test_vertical_victory(game):
    setup_board(game, [(0, 0), (1, 0), (2, 0), (3, 0)], "Y")
    assert game.check_vertical_victory() == "Y"


def test_forward_diagonal_victory(game):
    setup_board(game, [(0, 0), (1, 1), (2, 2), (3, 3)], "R")
    assert game.check_forward_diagonal_victory() == "R"


def test_backward_diagonal_victory(game):
    setup_board(game, [(3, 0), (2, 1), (1, 2), (0, 3)], "Y")
    assert game.check_backward_diagonal_victory() == "Y"


def setup_full_board_no_winner(game):
    draw_board = [
        ["Y", "Y", "Y", "R", "Y", "Y", "Y"],
        ["R", "R", "R", "Y", "R", "R", "R"],
        ["Y", "Y", "R", "R", "R", "Y", "R"],
        ["R", "R", "Y", "Y", "R", "R", "Y"],
        ["Y", "Y", "R", "R", "Y", "Y", "R"],
        ["Y", "Y", "R", "R", "Y", "R", "Y"],
    ]
    game.board = draw_board


def test_draw_condition(game):
    setup_full_board_no_winner(game)
    assert not game.check_horizontal_victory()
    assert not game.check_vertical_victory()
    assert not game.check_forward_diagonal_victory()
    assert not game.check_backward_diagonal_victory()
    assert game.check_draw()


def test_full_game_scenario(game):
    inputs = ["1", "2", "1", "2", "1", "2", "1"]
    with patch("builtins.input", side_effect=inputs):
        game.play()
        assert game.winner == "R"
