class Game:
    ROWS = ["a", "b", "c", "d", "e", "f"]
    COLUMNS = ["1", "2", "3", "4", "5", "6", "7"]
    PLAYER_COLORS = ["R", "Y"]
    LENGTH_TO_WIN = 4
    EMPTY = " "

    def __init__(self):
        self.winner: None | str = None
        self.board = [
            [self.EMPTY for _ in range(len(self.COLUMNS))]
            for _ in range(len(self.ROWS))
        ]

    def print_board(self):
        print(self.EMPTY * 4 + " ".join(self.COLUMNS))
        print(self.EMPTY * 4 + "- " * (len(self.COLUMNS)))
        for x, row_name in enumerate(self.ROWS):
            row_image = " ".join(self.board[x][y] for y in range(len(self.COLUMNS)))
            print(f"{row_name} | " + row_image)

    def play(self):
        while True:
            for index, color in enumerate(self.PLAYER_COLORS):
                self.print_board()
                is_ball_placed = False
                while not is_ball_placed:
                    try:
                        column = int(input(f"Player {index + 1} choose your column"))
                    except ValueError:
                        print(
                            "Column must be valid integer. Please enter valid column number"
                        )
                        continue
                    if column < 1 or column > len(self.COLUMNS):
                        print(
                            f"Column must be between 1 and {len(self.COLUMNS)}. Please enter valid column number"
                        )
                        continue
                    is_ball_placed = self.insert_ball(column=column, color=color)
                    if not is_ball_placed:
                        print(
                            f"The column {column} is already full. Please choose another one"
                        )
                self.check_victory()
                if self.winner:
                    self.print_board()
                    print(f"WINNER IS PLAYER {index + 1}: {self.winner}")
                    return
                if self.check_draw():
                    self.print_board()
                    print("The game is a draw")
                    return

    def insert_ball(self, column: int, color: str) -> bool:
        is_ball_placed = False
        for j in reversed(range(len(self.ROWS))):
            if self.board[j][column - 1] == self.EMPTY:
                self.board[j][column - 1] = color
                is_ball_placed = True
                break
        return is_ball_placed

    def check_victory(self):
        if winner := self.check_horizontal_victory():
            self.winner = winner
        if winner := self.check_vertical_victory():
            self.winner = winner
        if winner := self.check_forward_diagonal_victory():
            self.winner = winner
        if winner := self.check_backward_diagonal_victory():
            self.winner = winner

    def check_horizontal_victory(self):
        for row in self.board:
            for column in range(len(self.COLUMNS) - self.LENGTH_TO_WIN + 1):
                if self.is_winning_sequence(row[column : column + self.LENGTH_TO_WIN]):
                    return row[column]

    def check_vertical_victory(self):
        for column in range(len(self.COLUMNS)):
            for row in range(len(self.ROWS) - self.LENGTH_TO_WIN + 1):
                column_values = [
                    self.board[row + offset][column]
                    for offset in range(self.LENGTH_TO_WIN)
                ]
                if self.is_winning_sequence(column_values):
                    return self.board[row][column]

    def check_forward_diagonal_victory(self):
        for row in range(len(self.ROWS) - self.LENGTH_TO_WIN + 1):
            for column in range(len(self.COLUMNS) - self.LENGTH_TO_WIN + 1):
                diagonal_values = [
                    self.board[row + offset][column + offset]
                    for offset in range(self.LENGTH_TO_WIN)
                ]
                if self.is_winning_sequence(diagonal_values):
                    return self.board[row][column]

    def check_backward_diagonal_victory(self):
        for row in range(self.LENGTH_TO_WIN - 1, len(self.ROWS)):
            for column in range(len(self.COLUMNS) - self.LENGTH_TO_WIN + 1):
                diagonal_values = [
                    self.board[row - offset][column + offset]
                    for offset in range(self.LENGTH_TO_WIN)
                ]
                if self.is_winning_sequence(diagonal_values):
                    return self.board[row][column]

    def is_winning_sequence(self, sequence):
        first = sequence[0]
        return first != self.EMPTY and all(s == first for s in sequence)

    def check_draw(self):
        is_full = all(cell != self.EMPTY for row in self.board for cell in row)
        return is_full and not self.winner
